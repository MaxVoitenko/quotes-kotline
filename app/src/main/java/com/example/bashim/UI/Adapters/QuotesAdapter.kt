package com.example.bashim.UI.Adapters

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.bashim.R
import com.example.bashim.data.Quotes
import kotlinx.android.synthetic.main.source_item.view.*

class QuotesAdapter(list: MutableList<Quotes>): RecyclerView.Adapter<QuotesAdapter.holder>() {

    private val mItems: MutableList<Quotes> = list //вместо конструктора (this.item = item)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): holder {
        val layoutInflater = LayoutInflater.from(p0.context)
        val view = layoutInflater.inflate(R.layout.source_item, p0,false)
        return holder(view).listen{ position, type -> //прикрепляю слушатели к холдеру (НАВЕРНОЕ)
        }
    }

    override fun getItemCount(): Int {
        return mItems.size;
    }

    override fun onBindViewHolder(p0: holder, p1: Int) {
        val item = mItems[p1]
        p0.title.text = Html.fromHtml(item.htmlText) //запулил текст в вьюху
    }

    class holder(view: View): RecyclerView.ViewHolder(view){
        val title = view.text!! //знаки восклицания это првоерка на нул
    }



    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T { //НЕ ЗНАЮ ЗАЧЕМ todo
        itemView.setOnClickListener {
            event.invoke(adapterPosition, getItemViewType())
        }
        return this
    }
    operator fun get(position: Int): Quotes {//НЕ ЗНАЮ ЗАЧЕМ todo
        return mItems[position]
    }
}