package com.example.bashim.UI.Activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import butterknife.BindView
import butterknife.ButterKnife
import com.example.bashim.*
import com.example.bashim.Poviders.ChangeSourceListener
import com.example.bashim.Poviders.SearchRepository
import com.example.bashim.Poviders.SearchRepositoryProvider
import com.example.bashim.data.SourceOfQuotes
import com.example.bashim.UI.Adapters.SourcesAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class SourcesActivity : AppCompatActivity(), ChangeSourceListener {
    override fun sourceChanged(position: Int) {
//        Log.d("mLog", "from main = ${adapter[position]}")
        val intent = Intent(applicationContext, QuotesActivity::class.java)
        intent.putExtra(INTENT_NAME_NAME, adapter[position].name)
        intent.putExtra(INTENT_SITE_NAME, adapter[position].site)
        startActivity(intent)
    }


    @BindView(R.id.recycler)//библиотека типа ДАТАБИНДИНГ
    lateinit var recycler: RecyclerView
    lateinit var adapter: SourcesAdapter


    val compositeDisposable: CompositeDisposable = CompositeDisposable() //держит елементы в обьектах (на константное время)
    val repository: SearchRepository = SearchRepositoryProvider.provideSearchRepository()

    private val list: MutableList<SourceOfQuotes> = mutableListOf() //"мутейбл" чтобы можно было изменять

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)          //библиотека типа ДАТАБИНДИНГ

        recycler  = findViewById(R.id.recycler)//?? поидее можно безэтого, но что то не компилит нихуя


        val lm = LinearLayoutManager(this)
        lm.orientation = LinearLayoutManager.VERTICAL
        recycler.layoutManager = lm

        compositeDisposable.add(                //сюда
                repository.searchSourcesQuotes() //добавляем метод репозиторий (ищет цитатники)
                        .observeOn(AndroidSchedulers.mainThread())// подписываем на планировщик в главный поток (будем юзать только тут)
                        .subscribeOn(Schedulers.io())  //подписались все источники сообщений для ассинхронного? ввода вывода
                        .subscribe({result->   //подписались на результат
                            result.forEach{ //forEach = для каждого обьекта в резальте
                                list.addAll(it) //сюда получил результат
                            }
//                          Log.d("mLog", "gg")
                            adapter= SourcesAdapter(list)
                            adapter.addListener(this)
                            recycler.adapter = adapter
                        })
        )
    }

}
