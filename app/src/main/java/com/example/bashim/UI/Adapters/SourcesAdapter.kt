package com.example.bashim.UI.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.bashim.Poviders.ChangeSourceListener
import com.example.bashim.R
import com.example.bashim.data.SourceOfQuotes
import kotlinx.android.synthetic.main.source_item.view.*

class SourcesAdapter(listSource: MutableList<SourceOfQuotes>): RecyclerView.Adapter<SourcesAdapter.holder>() {

    private val mListener: MutableList<ChangeSourceListener> = mutableListOf() //кликеры (интерфейсы)
    private val mItems: MutableList<SourceOfQuotes> = listSource //вместо конструктора (this.item = item)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): holder {
        val layoutInflater = LayoutInflater.from(p0.context)
        val view = layoutInflater.inflate(R.layout.source_item, p0,false)
        return holder(view).listen{ position, type -> //прикрепляю слушатели к холдеру (НАВЕРНОЕ)
            changeSource(position)
        }
    }

    override fun getItemCount(): Int {
        return mItems.size;
    }

    override fun onBindViewHolder(p0: holder, p1: Int) {
        val item = mItems[p1]
        p0.title.text = item.desc //запулил текст в вьюху
    }

    class holder(view: View): RecyclerView.ViewHolder(view){
        val title = view.text!! //знаки восклицания это првоерка на нул
    }

    fun addListener(listener: ChangeSourceListener){ //добавляю листенер
        mListener.add(listener)
    }
    fun changeSource(position: Int){ //меняю
        mListener.forEach {
            it.sourceChanged(position)
        }
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T { //НЕ ЗНАЮ ЗАЧЕМ todo
        itemView.setOnClickListener {
            event.invoke(adapterPosition, getItemViewType())
        }
        return this
    }
    operator fun get(position: Int): SourceOfQuotes {//НЕ ЗНАЮ ЗАЧЕМ todo
        return mItems[position]
    }
}