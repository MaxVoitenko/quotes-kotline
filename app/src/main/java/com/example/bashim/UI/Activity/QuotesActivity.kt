package com.example.bashim.UI.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import butterknife.BindView
import butterknife.ButterKnife
import com.example.bashim.data.Quotes
import com.example.bashim.Poviders.SearchRepository
import com.example.bashim.Poviders.SearchRepositoryProvider
import com.example.bashim.R
import com.example.bashim.UI.Adapters.QuotesAdapter
import com.example.bashim.UI.Adapters.SourcesAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

const val INTENT_NAME_NAME = "name"
const val INTENT_SITE_NAME = "site"


class QuotesActivity : AppCompatActivity(){
    @BindView(R.id.recycler)//библиотека типа ДАТАБИНДИНГ
    lateinit var recycler: RecyclerView
    lateinit var adapter: SourcesAdapter


    val compositeDisposable: CompositeDisposable = CompositeDisposable() //держит елементы в обьектах (на константное время)
    val repository: SearchRepository = SearchRepositoryProvider.provideSearchRepository()

    private val list: MutableList<Quotes> = mutableListOf() //"мутейбл" чтобы можно было изменять

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)          //библиотека типа ДАТАБИНДИНГ

        recycler  = findViewById(R.id.recycler)//?? поидее можно безэтого, но что то не компилит нихуя


        val lm = LinearLayoutManager(this)
        lm.orientation = LinearLayoutManager.VERTICAL
        recycler.layoutManager = lm


        val site = intent.getStringExtra(INTENT_SITE_NAME)
        val name = intent.getStringExtra(INTENT_NAME_NAME)

//        Log.d("mLog", site!!.toString())
//        Log.d("mLog", name!!.toString())

        compositeDisposable.add(                //сюда
                repository.searchQuotes(site, name) //добавляем метод репозиторий (ищет цитатники)
                        .observeOn(AndroidSchedulers.mainThread())// подписываем на планировщик в главный поток (будем юзать только тут)
                        .subscribeOn(Schedulers.io())  //подписались все источники сообщений для ассинхронного? ввода вывода
                        .subscribe({ result ->
                            //подписались на результат
                            list.addAll(result) //сюда получил результат
                            recycler.adapter = QuotesAdapter(list)
                            Log.d("mLog", list.toString())
                        }))
    }

}
