package com.example.bashim.Poviders

import com.example.bashim.data.Quotes
import com.example.bashim.data.SourceOfQuotes
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface BashImApiService { // ИНТЕРФЕЙС ИЗ ЧЕГО СОСТОИТ ЗАПРОС

    @GET("api/get")
    fun searchQuotes(
        @Query("site")site:String,
        @Query("name")name:String,
        @Query("num")num:Int)
    :io.reactivex.Observable<List<Quotes>> //завернуть в обсервебал чтоб легко разобраться в активити

   @GET("api/sources")
    fun searchSources()
    :io.reactivex.Observable<List<List<SourceOfQuotes>>> //завернуть в обсервебал чтоб легко разобраться в активити

    companion object Factory {          //ЭТО СИНГЛТОН СОЗДАЕТ РЕТРОФИТ И ГСОН единажды, (чтоб не засорять память)
        fun create(): BashImApiService {
            val gson: Gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) //возвращает коллы (готовые адрессы)
                    .addConverterFactory(GsonConverterFactory.create(gson))//не совсем валидный Gson (отличие от Json), делает правильный гсон
                    .baseUrl("http://umorili.herokuapp.com/")  //базовый юрл
                    .build()
            return retrofit.create(BashImApiService::class.java)
        }
    }
}