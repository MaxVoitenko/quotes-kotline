package com.example.bashim.Poviders

import com.example.bashim.data.Quotes
import com.example.bashim.data.SourceOfQuotes

class SearchRepository(val apiService: BashImApiService){
    //прослойка которая поможет работаь с БашСервисом и синглТоном
    //Сам поисковик

    fun searchQuotes(site:String,name:String):io.reactivex.Observable<List<Quotes>>{
        return apiService.searchQuotes(site,name,10)
    }
  fun searchSourcesQuotes():io.reactivex.Observable<List<List<SourceOfQuotes>>>{
        return apiService.searchSources()
    }
}